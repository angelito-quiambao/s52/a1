function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.

    //Conditons:
        
        if (letter.length == 1) {
            // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
            for (let x = 0; x <= sentence.length; x++) {
                if(sentence[x] == letter){
                    result++;
                }
            }
            return result;
        }
        else{
            // If letter is invalid, return undefined.

            return undefined
        }
}


function isIsogram(text) {
    let result = text.toLowerCase();
    let count = 0;

    //Goal:
        // An isogram is a word where there are no repeating letters.

    //Check:
        // The function should disregard text casing before doing anything else.


    //Condition:
        // If the function finds a repeating letter, return false. Otherwise, return true.

    for(let x = 0; x <= result.length; x++){
        for(let y = 0; y <= result.length; y++){
            if(x != y){
                if(result[x] == result[y]){
                    count++;
                }
            }
        }
    }

    if (count == 0) {
        return  true
    }
    else{
        return false
    }
    
}

function purchase(age, price) {
    const discountedPrice = price * 0.8;
    const roundedPrice = discountedPrice.toFixed(2);

    //Conditions:
        
    if (age < 13) {
        // Return undefined for people aged below 13.
        return undefined
    }
    else if(age <= 21 || age >= 65){
        // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
        return roundedPrice.toString();
    }
    else{
        // Return the rounded off price for people aged 22 to 64.
        return price.toFixed(2).toString();
    }

    //Check:
        // The returned value should be a string.
    
}

function findHotCategories(items) {
    //Goal:
        // Find categories that has no more stocks.
        // The hot categories must be unique; no repeating categories.

    //Array for the test:
        // The passed items array from the test are the following:
        // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
        // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
        // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
        // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
        // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

        let noStocks = items.filter(item => item.stocks == 0)
        let category = noStocks.map(item => item.category)
        let uniqueCategory = [... new Set(category)]

        return uniqueCategory
    //Expected return must be array:
        // The expected output after processing the items array is ['toiletries', 'gadgets'].

    // Note:
        // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

}

function findFlyingVoters(candidateA, candidateB) {
    //Goal:
        // Find voters who voted for both candidate A and candidate B.

    //Array for the test:    
        // The passed values from the test are the following:
        // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
        // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // Expected return must be array:
        // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].

    //Note:
        // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
     
     let voters = [];

     let intersect = candidateA.map(data => {
        candidateB.filter(voter => {
            if(voter == data){
                voters.push(data);
            }
        })
     })

     return voters
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};